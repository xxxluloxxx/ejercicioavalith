package com.avalith.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.avalith.modelo.Persona;

public interface IPersonaRepo extends JpaRepository<Persona, Integer> {

}
