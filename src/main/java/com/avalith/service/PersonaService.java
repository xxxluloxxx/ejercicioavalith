package com.avalith.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.avalith.modelo.Persona;
import com.avalith.repo.IPersonaRepo;


@Controller
public class PersonaService {

	/*
	 * Inyectamos la dependencia de la interface
	 */
	@Autowired
	private IPersonaRepo repo;
	
	/*
	 * Metodo de guardar en la bd
	 */
	public void insertar(Persona p){
		repo.save(p);
	}
	
	/*
	 * Metodo de eliminar en la bd
	 */
	public void eliminar(Integer id){
		repo.deleteById(id);
	}
	
	/*
	 * Metodo de listar de la bd
	 */
	public List<Persona> listar(){
		return repo.findAll();
	}
	
}
