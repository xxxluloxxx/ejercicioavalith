package com.avalith.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.avalith.service.PersonaService;
import com.avalith.modelo.Persona;

import java.util.List;

/*
 * Clase que maneja los metodos rest que se usaran
 */
@RestController
@RequestMapping("/persona")
public class PersonaRest {

	/*
	 * Inyectamos la dependencia del controlador 
	 */
	@Autowired
	private PersonaService perservice;
	
	
	/*
	 * Metodo Rest para insertar un registro de una persona
	 */
	@PostMapping("/insertar")
	public void modificar(@RequestBody Persona p){
		perservice.insertar(p);
	}
	
	/*
	 * Metodo Rest para modificar un registro de una persona
	 */
	@PutMapping("/modificar")
	public void insertar(@RequestBody Persona p){
		perservice.insertar(p);
	}	
	
	/*
	 * Metodo Rest para eliminar un registro de una persona
	 */
	@DeleteMapping(value="/eliminar/{id}")
	public void eliminar(@PathVariable("id") Integer id){
		perservice.eliminar(id);
	}
	
	/*
	 * Metodo Rest para listar todos los registros de personas
	 */
	@GetMapping("/listar")
	public List<Persona> listar(){
		return perservice.listar();
	}
	
	
}
