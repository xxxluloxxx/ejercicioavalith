package com.avalith.modelo;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Persona {

	/*
	 * Este es el primary key de la entidad de bd
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int per_id;

	/*
	 * Nombres de la persona
	 */
	@Column(name = "per_nombre", length = 50)	
	private String per_nombre;

	/*
	 * Link de github
	 */
	@Column(name = "per_link", length = 50)
	private String per_link;
	
	/*
	 * Tecnologias usadas
	 */
	@Column(name="tecnologia")
	@ElementCollection(targetClass=String.class)
	private List<String> tecnologia;


	/*
	 * Metodos Getters and Setters
	 */
	public int getPer_id() {
		return per_id;
	}

	public void setPer_id(int per_id) {
		this.per_id = per_id;
	}

	public String getPer_nombre() {
		return per_nombre;
	}

	public void setPer_nombre(String per_nombre) {
		this.per_nombre = per_nombre;
	}

	public String getPer_link() {
		return per_link;
	}

	public void setPer_link(String per_link) {
		this.per_link = per_link;
	}

	public List<String> getTecnologia() {
		return tecnologia;
	}

	public void setTecnologia(List<String> tecnologia) {
		this.tecnologia = tecnologia;
	}	

}
