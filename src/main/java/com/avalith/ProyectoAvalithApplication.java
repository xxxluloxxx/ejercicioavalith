package com.avalith;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProyectoAvalithApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProyectoAvalithApplication.class, args);
	}

}
