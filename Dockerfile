FROM openjdk:8-jdk-alpine
MAINTAINER avalith.com
VOLUME /tmp
EXPOSE 8080
ADD build/libs/ProyectoAvalith-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]