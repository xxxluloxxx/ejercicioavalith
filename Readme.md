# README #

El aplicativo desarrollado en SpringBoot permite el registro de datos de desarrolladores de software y las tecnologías que conocen, estos datos son guardados en una base de datos POSTGRESQL.
La aplicación expone 4 servicio REST como son: de guardar, modificar, listar y eliminar.
Estas pruebas fueron realizadas con Postman.

### Datos básicos del aplicativo ###

* Puerto: 8080
* Versión: 1.0

### Datos básicos de la BD POSTGRESQL ###

* database=POSTGRESQL
* datasource.url=jdbc:postgresql://dbpostgresql:5432/mydb
* username=postgres
* password=sistemas

### Datos de la persona ###

* **Nombre**
* * Nombre completos de la persona (50 caracteres)
* **Link**
* * Link de cuenta de github (50 caracteres)
* **Tecnología**
* * Una lista de tecnologías


### Servicios REST expuestos ###

* **Ingresar nuevos datos**
* * Metodo: POST
* * URL: http://localhost:8080/persona/insertar
* * URL heroku: https://avalith-ejercicio-final.herokuapp.com/persona/insertar
* * JSON:
{
	"per_nombre" : "Paul Arias",
	"per_link" : "[https://bitbucket.org/xxxluloxxx](https://bitbucket.org/xxxluloxxx/ejercicioavalith/src/master/)",
	"tecnologia": ["Git", "C++", "Java", "Node"]
}



* **Modificar datos existentes**
* * Metodo: PUT
* * URL: http://localhost:8080/persona/modificar
* * URL heroku: https://avalith-ejercicio-final.herokuapp.com/persona/modificar
* * JSON:
{
"per_id" : 1,
"per_nombre" : "Jhordy Paul Arias Espinoza",
"per_link" : "[https://bitbucket.org/xxxluloxxx](https://bitbucket.org/xxxluloxxx/ejercicioavalith/src/master/)",
"tecnologia": ["Git", "C++", "Java", "Node", "Sql"]
}


* **Eliminar un registro**
* * Metodo: DELETE
* * URL: http://localhost:8080/persona/eliminar/1
* * URL heroku: https://avalith-ejercicio-final.herokuapp.com/persona/eliminar/1
* * Se pasa como parámetro el id de la persona a eliminar

* **Listar los registroS**
* * Metodo: GET
* * URL: http://localhost:8080/persona/listar
* * URL heroku: https://avalith-ejercicio-final.herokuapp.com/persona/listar
* * Lista todas las personas dentro de la BD


### Construir aplicación ###

* Construir el ejecutable ".jar" que sera utilizado en el docker para su despliegue localmente.

* * ./gradlew build -x test


* Construir el dockerizado a partir del dockerfile y el docker-compose, la siguiente linea de comando realizara el docker de la aplicación para su ejecución localmente.

* * docker-compose up --build


### El aplicativo se encuentra desplegado en Heroku ###

* Se desplegó el proyecto el día jueves 04 de junio por lo que estará habilitado para consultas durante el tiempo que la versión gratuita lo permita
* El url es: https://avalith-ejercicio-final.herokuapp.com